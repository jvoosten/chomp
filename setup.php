<?php

$sql_statements = [
  1 => [
    "CREATE TABLE website (
      id INTEGER PRIMARY KEY,
      nickname TEXT,
      description TEXT,
      starturl TEXT
    )",
    "CREATE TABLE snapshot (
      id INTEGER PRIMARY KEY,
      website_id INTEGER,
      begin_snap INTEGER,  -- when snapshotting begin
      end_snap INTEGER,    -- when it ended
      code_revision TEXT,
      page_count INTEGER
    )",
    "CREATE INDEX snap_web ON snapshot(website_id)",
    "CREATE TABLE page (
      id INTEGER PRIMARY KEY,
      snapshot_id INTEGER,
      captured_on INTEGER,
      status INTEGER,   -- parsed or not
      path TEXT,
      html TEXT
    )",
    "CREATE INDEX page_snap ON page(snapshot_id)",
    "CREATE INDEX page_path ON page(path)"
  ],
];

$db = new SQLite3 ('chomp.sqlite3');
foreach ($sql_statements as $version => $st)
{
  $db->exec ("BEGIN TRANSACTION");
  foreach ($st as $t)
  {
    if (!$db->exec ($t))
    {
      printf ("Failed to execute \"%s\", error = %s\n", $t, $db->lastErrorMsg);
      $db->exec ("ROLLBACK");
      return 1;
    }
  }
  $db->exec ("COMMIT TRANSACTION");
}
