<?php

namespace Chomp
{

  /**
   * @brief Class to scan a whole site
   */
  class Scanner
  {
    private $db = null;
    private $start_url = null;
    private $snapshot_id = null;

    private $url_cache = array();

    const STATUS_NEW = 0;
    const STATUS_GRABBED = 1;
    const STATUS_ERROR = 9;

    /**
     * @param object $db Database (PDO object)
     * @param string $start Start URL
     * @param int $snap_id Snapshot ID
     *
     * Constructor; stored database, start URL and snapshot ID. A trailing '/'
     * from the start is removed as that is part of the path.
     */
    public function __construct ($db, $start, $snap_id)
    {
      $this->db = $db;
      // Strip trailing / from start URL
      $end = substr ($start, -1);
      if ($end == '/')
      {
        $start = substr ($start, 0, -1);
      }
      $this->start_url = $start;
      $this->snapshot_id = $snap_id;

      // Load existing URLs into cache
      $u = $this->db->prepare ('SELECT id,path FROM page WHERE snapshot_id = :snapshot');
      $u->bindValue('snapshot', $snap_id);
      if (!$u->execute ())
      {
        printf ("Failed to get existing URLs for cache.\n");
      }
      else
      {
        while (($row = $u->fetch(\PDO::FETCH_ASSOC)) !== false)
        {
          $this->url_cache[$row['path']] = $row['id'];
        }
        printf ("Loaded %d URLs in cache\n", sizeof($this->url_cache));
      }
    }

    /**
     * @brief Add a path inside the site to scan
     * @param string $path
     * @param int $status Initial status
     * Creates an entry in the snapshot database to scan.
     *
     * Note: the path should start with a /
     */
    public function add_path ($path, $status = 0)
    {
      $p = $this->db->prepare ('INSERT INTO page (snapshot_id, status, path) VALUES (:snapshot, :status, :path)');
      $p->bindValue ('snapshot', $this->snapshot_id);
      $p->bindValue ('status', $status);
      $p->bindValue ('path', $path);
      $p->execute ();

      // Add to cache too
      $this->url_cache[$path] = $this->db->lastInsertId();
    }

    /**
     * @brief Scan new pages
     * @return int Number of new pages found
     */
    public function scan ()
    {
      $count = 0;
      $q = $this->db->prepare ('SELECT id,path FROM page WHERE snapshot_id = :snapshot AND status = :status');
      $q->bindValue ('snapshot', $this->snapshot_id);
      $q->bindValue ('status', self::STATUS_NEW);
      $q->execute ();
      $pages = $q->fetchAll (\PDO::FETCH_ASSOC);
      foreach ($pages as $page)
      {
        $count += $this->scan_page ($page['id'], $page['path']);
      }
      return $count;
    }

    /**
     * @brief Load page from server, store content and find new URLs
     * @param int $id ID of the page
     * @param string $path Relative path on server
     * @return int Number of new pages found
     */
    protected function scan_page ($id, $path)
    {
      printf ("@ %s\n", $path);
      $ret = 0;

      // Fetch page with cURL
      $url = $this->start_url . $path;
      $c = curl_init ($url);
      curl_setopt ($c, CURLOPT_HEADER, 0);
      curl_setopt ($c, CURLOPT_RETURNTRANSFER, true);
      $html = curl_exec ($c);

      if (curl_errno ($c))
      {
        printf ("Failed to get %s\n", $url);
        $u = $this->db->prepare ('UPDATE page SET status = :status WHERE id = :id');
        $u->bindValue ('status', -curl_errno ($c));
        $u->bindValue ('id', $id);
        $u->execute ();
        curl_close ($c);
        return 0;
      }

      curl_close ($c);

      $u = $this->db->prepare ('UPDATE page SET status = :status, captured_on = :captured, html = :html WHERE id = :id');
      $u->bindValue ('captured', time ());
      $u->bindValue ('html', $html);
      $u->bindValue ('id', $id);

      // We now have the full HTML; parse into a DOM and see if we can make something out of it
      $d = new \DOMDocument();
      if (false === $d->loadHTML ($html))
      {
        printf ("Failed to parse HTML in %s\n", $url);
        $u->bindValue ('status', self::STATUS_ERROR);
        $u->execute ();
      }
      else
      {
        $u->bindValue ('status', self::STATUS_GRABBED);
        $u->execute ();
        $ret = 0;
        $skipped = 0;
        $dups = 0;
        $this->walk_page ($d, $ret, $skipped, $dups);
        printf ("& %d new URLS, %d skipped, %d duplicates\n", $ret, $skipped, $dups);
      }
      return $ret;
    }

    /**
     * @brief Recursive parsing of nodes in HTML page
     * @param DOMElement $node Node in document
     * @param int $new_urls Newly found URLs
     * @param int $skipped Skipped URLs, outside of our domain
     * @param int $dups Number of URLs we already have
     */
    protected function walk_page ($node, &$new_urls, &$skipped, &$dups)
    {
      $path = '';

      foreach ($node->childNodes as $n)
      {
        if (XML_ELEMENT_NODE == $n->nodeType)
        {
          //printf ("%s<%s> %d\n", "  ", $n->nodeName, $n->nodeType);
          if ('a' == $n->nodeName)
          {
            // We have an a-tag... get href
            $href = $n->getAttribute ('href');
            //printf ("@ %s\n", $href);

            $u = parse_url ($href);
            if (empty ($u) || strpos($href, '<b>Notice</b>') > 0)
            {
              continue;
            }
            $path = isset ($u['path']) ? $u['path'] : null;
            $host = isset ($u['host']) ? $u['host'] : null;
            if (empty ($path) || '/' == $path) // we already did the root
            {
              $dups++;
              continue;
            }
            if (!empty ($host))
            {
              // Compare start URL to our start URL; if it is outside of that, do not follow
              if (stripos ($href, $this->start_url, 0) !== 0)
              {
                $skipped++;
                continue;
              }
            }

            // Did we find this path already?
            if (array_key_exists($path, $this->url_cache))
            {
              $dups++;
            }
            else
            {
              printf ("+ adding %s\n", $path);
              $this->add_path ($path);
              $new_urls++;
            }
          }

          // Recurse
          $this->walk_page ($n, $new_urls, $skipped, $dups);
        }
      }
    }
  }  //..walk_page

} // .. namespace
?>