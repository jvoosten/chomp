<?php

include 'scanner.php';



function scan_site ($db, $nick, $mode)
{
  $s = $db->prepare ('SELECT * FROM website WHERE nickname = :nickname');
  $s->bindValue ('nickname', $nick);
  if (!$s->execute ())
  {
    printf ("Failed to query site '%s'.\n", $nick);
    return false;
  }

  $site = $s->fetch (PDO::FETCH_ASSOC);
  if (false === $site)
  {
    printf ("Website '%s' not found in database.\n", $nick);
    return false;
  }

  // Check mode; if 'continue', re-start latest snapshot
  $snap_id = null;
  if ('continue' == $mode)
  {
    $q = $db->prepare ('SELECT id FROM snapshot WHERE website_id = :website_id ORDER BY id DESC LIMIT 1');
    $q->bindValue ('website_id', $site['id']);
    $q->execute ();
    $s = $q->fetch (PDO::FETCH_ASSOC);
    if (empty ($s))
    {
      printf ("Website has no prior snapshot\n");
      return false;
    }
    $snap_id = $s['id'];
    printf ("Continue with snapshot %d\n", $snap_id);
    $q->closeCursor ();
  }
  else
  {
    // Create new snapshot
    $q = sprintf ('INSERT INTO snapshot (website_id, begin_snap) VALUES (%d, %d)', $site['id'], time ());
    $db->exec ($q);
    $snap_id = $db->lastInsertId ();
    printf ("Created new snapshot %d\n", $snap_id);
  }

  $scanner = new Chomp\Scanner ($db, $site['starturl'], $snap_id);
  if ('new' == $mode)
  {
    $scanner->add_path ('/');
  }

  $total = 0;
  //do
  //{
    $new_pages = $scanner->scan ();
    $total += $new_pages;
  //} while ($new_pages > 0);
  printf("Scanned %d pages\n", $total);

  // We're done, update end timestamp
  $p = $db->prepare("UPDATE snapshot SET end_snap = :time WHERE id = :id");
  $p->bindValue (':time', time());
  $p->bindValue(':id', $snap_id);

  return $total;
}

function usage ()
{
  print "Usage: <chomp> [options] command [args...]\n";
  print "\n";
  print "Options\n";
  print "  -c, --continue         Continue last scan\n";
  print "\n";
  print "Commands\n";
  print "  scan site-nick         Scan site given after command\n";
}

/* Main */
// Do not show errors while parsing HTML
libxml_use_internal_errors (true);

$mode = 'new'; // new snapshot vs continue last
$command = null;
$site_nick = null;

if ($argc <= 1)
{
  usage ();
  return 1;
}

$last_arg = 0;
$opts = getopt ("c", ["continue"], $last_arg);

if (isset ($opts['c']) || isset ($opts['continue']))
{
  $mode = 'continue';
}
if ($last_arg == $argc)
{
  print "Error: missing command.\n";
  usage ();
  return 2;
}

// Time to open the database
$db = new PDO ('sqlite:' . __DIR__ . '/chomp.sqlite3');
$db->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

try
{
  $command = $argv[$last_arg];
  $last_arg++;
  switch ($command)
  {
    case 'scan':
      if ($last_arg == $argc)
      {
        printf ("Scan command missing site-nick\n");
        return 2;
      }
      $processed = scan_site ($db, $argv[$last_arg], $mode);
      printf ("Processed %d pages\n", $processed);
      $last_arg++;
      break;

    default:
      print "Error: unknown command " . $command . "\n";
      usage ();
      return 2;
  }
}
catch (Exception $e)
{
  printf ("PDO Exception occured in %s, line %d: [%d] %s\n", $e->getFile (), $e->getLine (), $e->getCode (), $e->getMessage ());
  printf ("%s\n", $e->getTraceAsString ());
}
?>